﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Contrib;

namespace VMailRest
{
    public class VMailRestHelper
    {
        #region Vars
#if LOCALDEBUG
        public const string BaseUrl = "http://localhost:3000/";
#elif REMOTEDEBUG
        public const string BaseUrl = "https://obscure-reef-3001.azurewebsites.net/";
#else
        public const string BaseUrl = "https://obscure-reef-3001.azurewebsites.net/";
#endif
        private static readonly CookieContainer CookieContainer = new CookieContainer();
        private static readonly RestClient RestClient = new RestClient(BaseUrl){CookieContainer = CookieContainer};

        public string Username { get; private set; }
        public string Password { get; private set; }

        #endregion

        
        #region cTor

        public VMailRestHelper(string userName, string password)
        {
            Username = userName;
            Password = password;
        }

        #endregion


        #region Methods
        private IRestResponse MakeRequest(RestRequest request)
        {
            request.RequestFormat = DataFormat.Json;
            dynamic loginAttempt = new ExpandoObject();
            loginAttempt.username = Username;
            loginAttempt.password = Password;
            var json = request.JsonSerializer.Serialize(loginAttempt);
            request.AddParameter("application/json; charset=utf-8", json, ParameterType.RequestBody);
            return RestClient.Execute(request);
        }
        private IRestResponse MakeRequest(RestRequest request, dynamic vmail)
        {
            request.RequestFormat = DataFormat.Json;
            dynamic loginAttempt = new ExpandoObject();
            loginAttempt.username = Username;
            loginAttempt.password = Password;
            loginAttempt.Id = vmail.Id;
            loginAttempt.Subject = vmail.Subject;
            var json = request.JsonSerializer.Serialize(loginAttempt);
            request.AddParameter("application/json; charset=utf-8", json, ParameterType.RequestBody);
            return RestClient.Execute(request);
        }
        public PlayRequest GetCallRequests()
        {
            var request = new RestRequest("CallRequest", Method.GET);
            var response = MakeRequest(request, Username, Password);
            if (string.IsNullOrEmpty(response.Content) || response.Content.Equals("false"))
                return new PlayRequest(string.Empty, string.Empty);
            dynamic callReq = JsonConvert.DeserializeObject<ExpandoObject>(response.Content.Substring(1, response.Content.Length-2));
            return new PlayRequest(callReq.messageid, callReq.phoneNumber);
        }
        public IRestResponse LoginApp()
        {
            var request = new RestRequest("loginApp", Method.POST);
            return MakeRequest(request);
        }

        public IRestResponse DashBoard()
        {
            var request = new RestRequest("Dashboard", Method.GET);
            return MakeRequest(request);
        }

        public IRestResponse AddVmail(dynamic vMail)
        {
            var request = new RestRequest("vmails", Method.POST);
            return MakeRequest(request, vMail);
        }

        public IRestResponse DeleteVmail(dynamic vMail)
        {
            var request = new RestRequest("vmails", Method.DELETE);
            return MakeRequest(request, vMail);
        }

        public IRestResponse GetVmail(dynamic vMail)
        {
            var request = new RestRequest("vmails", Method.GET);
            return MakeRequest(request, vMail);
        }
        #endregion


        
        #region StaticMethods

        public static PlayRequest GetCallRequests(string userName, string password)
        {
            var request = new RestRequest("CallRequest", Method.GET);
            var response = MakeRequest(request, userName, password);
            if (string.IsNullOrEmpty(response.Content) || response.Content.Equals("false"))
                return new PlayRequest(string.Empty,string.Empty);
            dynamic callReq = JsonConvert.DeserializeObject<ExpandoObject>(response.Content);
            return new PlayRequest(callReq.messageid,callReq.phoneNumber);
        }

        private static IRestResponse MakeRequest(RestRequest request, string userName, string password)
        {
            request.RequestFormat = DataFormat.Json;
            dynamic loginAttempt = new ExpandoObject();
            loginAttempt.username = userName;
            loginAttempt.password = password;
            var json = request.JsonSerializer.Serialize(loginAttempt);
            request.AddParameter("application/json; charset=utf-8", json, ParameterType.RequestBody);
            return RestClient.Execute(request);
        }
        private static IRestResponse MakeRequest(RestRequest request, string userName, string password,dynamic vmail)
        {
            request.RequestFormat = DataFormat.Json;
            dynamic loginAttempt = new ExpandoObject();
            loginAttempt.username = userName;
            loginAttempt.password = password;
            loginAttempt.Id = vmail.Id;
            loginAttempt.Subject = vmail.Subject;
            var json = request.JsonSerializer.Serialize(loginAttempt);
            request.AddParameter("application/json; charset=utf-8", json, ParameterType.RequestBody);
            return RestClient.Execute(request);
        }

        public static LoginResult LoginApp(string userName, string password)
        {
            var request = new RestRequest("loginApp", Method.POST);
            var response =  MakeRequest(request, userName, password);
            if (string.IsNullOrEmpty(response.Content) || response.Content.Equals("false"))
                return new LoginResult(response, false);
            return new LoginResult(response, true);
        }

        private static bool CheckExpandoPropertyExists(string property,ExpandoObject expandoObject)
        {
            if (!string.IsNullOrEmpty(property) && ((IDictionary<String, object>) expandoObject).ContainsKey(property))
            {
                return true;
            }
            return false;
        }

        public static IRestResponse DashBoard(string userName, string password)
        {
            var request = new RestRequest("Dashboard", Method.GET);
            return MakeRequest(request, userName, password);
        }

        public static IRestResponse AddVmail(string userName, string password, dynamic vMail)
        {
            var request = new RestRequest("vmails", Method.POST);
            return MakeRequest(request, userName, password,vMail);
        }

        public static IRestResponse DeleteVmail(string userName, string password, dynamic vMail)
        {
            var request = new RestRequest("vmails", Method.DELETE);
            return MakeRequest(request, userName, password, vMail);
        }
        #endregion



    }

    public struct LoginResult
    {
        public IRestResponse Response { get; private set; }
        public bool WasSuccessful { get; private set; }

        internal LoginResult(IRestResponse response, bool wasSuccess) : this()
        {
            Response = response;
            WasSuccessful = wasSuccess;
        }
    }

    public struct PlayRequest
    {
        public string VmailId { get; private set; }
        public string PhoneNumber { get; private set; }

        internal PlayRequest(string vmailId, string phoneNumber)
            : this()
        {
            VmailId = HttpUtility.HtmlDecode(vmailId).ToLower();
            PhoneNumber = HttpUtility.HtmlDecode(phoneNumber).ToLower();
        }
    }

}
