﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace VMailAddin
{
    public static class SettingsHelper
    {
        public static void SaveUserNameAndPassword(string userName, string password)
        {
            Properties.Settings.Default.userName = EncryptionHelper.EncryptDPAPI(userName,DataProtectionScope.CurrentUser);
            Properties.Settings.Default.password = EncryptionHelper.EncryptDPAPI(password,DataProtectionScope.CurrentUser);
            Properties.Settings.Default.Save();
        }

        public static string GetUserName()
        {
            return EncryptionHelper.DecryptStringDPAPI(Properties.Settings.Default.userName,DataProtectionScope.CurrentUser);
        }

        public static string GetPassword()
        {
            return EncryptionHelper.DecryptStringDPAPI(Properties.Settings.Default.password,DataProtectionScope.CurrentUser);
        }

        public static void SetAutoLogin(bool value)
        {
            Properties.Settings.Default.autoLogin = value;
            Properties.Settings.Default.Save();
        }

        public static bool GetAutoLoin()
        {
            return Properties.Settings.Default.autoLogin;
        }

        public static void SetSaveCreds(bool? value)
        {
            if (value != null && !(bool)value)
            {
                SaveUserNameAndPassword("","");
            }
            if (value != null) Properties.Settings.Default.saveCreds = (bool)value;
            Properties.Settings.Default.Save();
        }


        public static class EncryptionHelper
        {
            // ReSharper disable InconsistentNaming
            //private static readonly byte[] s_aditionalEntropy = { 8, 5, 7, 6, 7, 4, 4, 3 };
            // ReSharper restore InconsistentNaming

            public static byte[] GetEntropyBytes()
            {
                using (var rng = new RNGCryptoServiceProvider())
                {
                    var data = new byte[8];
                    rng.GetBytes(data);
                    return data;
                }
            }

            public static string EncryptDPAPI(string stringToProtect,DataProtectionScope dataProtectionScope)
            {
                    try
                    {
                        // Encrypt the data using DataProtectionScope.CurrentUser. The result can be decrypted 
                        //  only by the same current user. 
                        var bytes = GetEntropyBytes();
                        var encData = Convert.ToBase64String(ProtectedData.Protect(GetBytes(stringToProtect),
                                                                            bytes,
                                                                            dataProtectionScope));
                        var ret = string.Format("{0}###{1}",GetString(bytes),encData);
                        return ret;
                    }
                    catch (CryptographicException e)
                    {
                        return null;
                    }
            }

            public static string[] EncryptTDES(string stringToEncrypt, string decryptedPassphrase)
            {
                var rngGenerator = new RNGCryptoServiceProvider();
                var saltybytes = new byte[8];
                rngGenerator.GetBytes(saltybytes);
                var salt = Convert.ToBase64String(saltybytes);

                byte[] results;
                var utf8 = new UTF8Encoding();

                // Step 1. We hash the passphrase using MD5
                // We use the MD5 hash generator as the result is a 128 bit byte array
                // which is a valid length for the TripleDES encoder we use below

                var hashProvider = new MD5CryptoServiceProvider();
                byte[] tdesKey = hashProvider.ComputeHash(utf8.GetBytes(decryptedPassphrase));

                // Step 2. Create a new TripleDESCryptoServiceProvider object
                var tdesAlgorithm = new TripleDESCryptoServiceProvider();

                // Step 3. Setup the encoder
                tdesAlgorithm.Key = tdesKey;
                tdesAlgorithm.Mode = CipherMode.ECB;
                tdesAlgorithm.Padding = PaddingMode.PKCS7;

                // Step 4. Convert the input string to a byte[]
                byte[] dataToEncrypt = utf8.GetBytes(salt + stringToEncrypt);

                // Step 5. Attempt to encrypt the string
                try
                {
                    ICryptoTransform encryptor = tdesAlgorithm.CreateEncryptor();
                    results = encryptor.TransformFinalBlock(dataToEncrypt, 0, dataToEncrypt.Length);
                }
                finally
                {
                    // Clear the TripleDes and Hashprovider services of any sensitive information
                    tdesAlgorithm.Clear();
                    hashProvider.Clear();
                }

                // Step 6. Return the encrypted string as a base64 encoded string
                return new string[] { salt, Convert.ToBase64String(results) };
            }

            private static byte[] GetBytes(string str)
            {
                    var bytes = new byte[str.Length * sizeof(char)];
                    Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
                    return bytes;
            }
            public static string DecryptStringTDES(string message, string passphrase, string salt)
            {
                byte[] results;
                var utf8 = new UTF8Encoding();

                // Step 1. We hash the passPhrase using MD5
                // We use the MD5 hash generator as the result is a 128 bit byte array
                // which is a valid length for the TripleDES encoder we use below

                var hashProvider = new MD5CryptoServiceProvider();
                byte[] tdesKey = hashProvider.ComputeHash(utf8.GetBytes(passphrase));

                // Step 2. Create a new TripleDESCryptoServiceProvider object
                var tdesAlgorithm = new TripleDESCryptoServiceProvider { Key = tdesKey, Mode = CipherMode.ECB, Padding = PaddingMode.PKCS7 };

                // Step 3. Setup the decoder
                tdesAlgorithm.Key = tdesKey;
                tdesAlgorithm.Mode = CipherMode.ECB;
                tdesAlgorithm.Padding = PaddingMode.PKCS7;

                // Step 4. Convert the input string to a byte[]
                byte[] dataToDecrypt = Convert.FromBase64String(message);

                // Step 5. Attempt to decrypt the string
                try
                {
                    var decryptor = tdesAlgorithm.CreateDecryptor();
                    results = decryptor.TransformFinalBlock(dataToDecrypt, 0, dataToDecrypt.Length);
                }
                finally
                {
                    // Clear the TripleDes and Hash provider services of any sensitive information
                    tdesAlgorithm.Clear();
                    hashProvider.Clear();
                }

                // Step 6. Return the decrypted string in UTF8 format
                var decryptedPassword = utf8.GetString(results);

                if (salt != null)
                {
                    var index = decryptedPassword.IndexOf(salt, StringComparison.Ordinal);
                    decryptedPassword = (index < 1) ? decryptedPassword.Remove(index, salt.Length) : null;
                }

                return decryptedPassword;
            }

            public static string DecryptStringDPAPI(string encryptedString, DataProtectionScope dataProtectionScope)
            {
                    try
                    {
                        //Decrypt the data using DataProtectionScope.LocalMachine. 
                        if (string.IsNullOrEmpty(encryptedString)) return string.Empty;
                        if (!encryptedString.Contains("###")) return string.Empty;
                        var salt = GetBytes(encryptedString.Split(new string[] {"###"}, StringSplitOptions.None)[0]);
                        
                        var hash = encryptedString.Split(new string[] {"###"}, StringSplitOptions.None)[1];
                        return GetString(ProtectedData.Unprotect(Convert.FromBase64String(hash), salt, dataProtectionScope));
                    }
                    catch (CryptographicException e)
                    {
                        return null;
                    }
                
            }

            private static string GetString(byte[] bytes)
            {
                
                    var chars = new char[bytes.Length / sizeof(char)];
                    Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
                    return new string(chars);
            }
        }
    }
}
