﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;

namespace VMailAddin
{
    /// <summary>
    /// Interaction logic for MainView.xaml
    /// </summary>
    public partial class MainView : UserControl
    {
        private string Username { get; set; }
        private string Password { get; set; }
        public MainView()
        {
            InitializeComponent();
            if (!Properties.Settings.Default.saveCreds || !Properties.Settings.Default.autoLogin) return;
            if (string.IsNullOrEmpty(Properties.Settings.Default.userName) ||
                string.IsNullOrEmpty(Properties.Settings.Default.password))
                return;
            txtUsername.Text = SettingsHelper.GetUserName();
            txtPassword.Password = SettingsHelper.GetPassword();
            btnLogin_Click(this,null);
        }

        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (txtUsername.Text == string.Empty || txtUsername.Text == "UserName")
                txtUsername.Text = string.Empty;
        }

        private void txtUsername_LostFocus(object sender, RoutedEventArgs e)
        {
            if (txtUsername.Text == string.Empty)
                txtUsername.Text = "UserName";
        }

        private void btnRegister_Click(object sender, RoutedEventArgs e)
        {
#if REMOTEDEBUG
            Process.Start("https://obscure-reef-3001.azurewebsites.net/register");
#elif LOCALDEBUG
            Process.Start("http://localhost:3001/register");
#else
            Process.Start("https://obscure-reef-3001.azurewebsites.net/register");
#endif
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtPassword.Password) || string.IsNullOrEmpty(txtUsername.Text))
            {
                MessageBox.Show("Username and password are required", "Login Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            var loginResponse = VMailRest.VMailRestHelper.LoginApp(txtUsername.Text, txtPassword.Password);
            if (loginResponse.WasSuccessful)
            {
                SettingsHelper.SaveUserNameAndPassword(txtUsername.Text,txtPassword.Password);
                DockPanelMain.Visibility = Visibility.Hidden;
                DockPanelLoggedIn.Visibility = Visibility.Visible;
                VmailPage.Password = txtPassword.Password;
                VmailPage.UserName = txtUsername.Text;
                VmailPage.StartMonitoringVmails();
            }
            else
            {
                txtPassword.Password = string.Empty;
                txtUsername.Text = string.Empty;
                MessageBox.Show("Invalid Login Attempt", "Login Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }


        }

        private void btnLogOff_Click(object sender, RoutedEventArgs e)
        {
            DockPanelLoggedIn.Visibility = System.Windows.Visibility.Hidden;
            DockPanelMain.Visibility = System.Windows.Visibility.Visible;
        }

        private void btnOpenWebSite_Click(object sender, RoutedEventArgs e)
        {
#if REMOTEDEBUG
            Process.Start("https://obscure-reef-3001.azurewebsites.net/");
#elif LOCALDEBUG
            Process.Start("http://localhost:3001/");
#else
            Process.Start("https://obscure-reef-3001.azurewebsites.net/");
#endif
        }

        private void chkSaveCreds_Checked(object sender, RoutedEventArgs e)
        {
            var cb = sender as CheckBox;
            if (cb == null) return;
            if (cb.IsChecked == null) return;
            SettingsHelper.SetSaveCreds((cb.IsChecked != null && ((bool)cb.IsChecked)));
            if (chkAutoLogon == null) return;
            if (cb.IsChecked != null) chkAutoLogon.IsEnabled = (bool) cb.IsChecked;
        }

        private void chkAutoLogon_Checked(object sender, RoutedEventArgs e)
        {
            var cb = sender as CheckBox;
            if (cb == null) return;
            if (cb.IsChecked == null) return;
            SettingsHelper.SetAutoLogin((bool)cb.IsChecked);
        }

        private void chkSaveCreds_Unchecked(object sender, RoutedEventArgs e)
        {
            var cb = sender as CheckBox;
            if (cb == null) return;
            
            if (cb.IsChecked == null) return;
            SettingsHelper.SetSaveCreds((cb.IsChecked != null && ((bool)cb.IsChecked)));
            if (chkAutoLogon == null) return;
            if (cb.IsChecked != null) chkAutoLogon.IsEnabled = (bool)cb.IsChecked;
        }

        private void chkAutoLogon_Unchecked(object sender, RoutedEventArgs e)
        {
            var cb = sender as CheckBox;
            if (cb == null) return;
            if (cb.IsChecked == null) return;
            SettingsHelper.SetAutoLogin((bool)cb.IsChecked);
        }






    }

}
