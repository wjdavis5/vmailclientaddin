﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Web;
using ININ.IceLib.Connection;
using ININ.InteractionClient.AddIn;
using ININ.IceLib.UnifiedMessaging;
using Timer = System.Timers.Timer;
using VMailRest;

namespace VMailAddin
{
    public class VmailPage : AddInWindow
    {
        public static ICallService CallService { get; set; }

        public static int hdl = I3Trace.initialize_topic("VisualVMailClientAddin.Main", 80);

        public static ITraceContext _trace;
        public static string UserName { get; set; }
        public static string Password { get; set; }
        private static readonly Timer CheckCommands = new Timer(4000){AutoReset = true};
        private static readonly Timer RefreshVmailTimer = new Timer(10000){AutoReset = true};
        private static UnifiedMessagingManager _uMm;
        private static Session _session;
        private static readonly object Locker = new object();
        private static VMailRestHelper VMailRest { get; set; }
        private static readonly Dictionary<string, VoicemailMessage> VoicemailMessages =
            new Dictionary<string, VoicemailMessage>();

        



        public VmailPage()
        {
            RefreshVmailTimer.Elapsed -= RefreshVmailTimerOnElapsed;
           RefreshVmailTimer.Elapsed += RefreshVmailTimerOnElapsed;   
        }

        private static void RefreshVmailTimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            Task.Factory.StartNew(() =>
            {
                try
                {   
                    _uMm.RefreshVoicemailCache(-1);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            });
        }

        private static void UMmOnNewVoicemail(object sender, VoicemailEventArgs voicemailEventArgs)
        {
            if(!voicemailEventArgs.Message.Subject.Equals("IC Voicemail: Voicemail Notification"))
                AddMessage(voicemailEventArgs.Message);
        }

        private static void AddMessage(VoicemailMessage voicemailMessage)
        {
            Thread.Sleep(1);
            if (!VoicemailMessages.ContainsKey(RestSharp.Contrib.HttpUtility.HtmlDecode(voicemailMessage.Id).ToLower()))
                lock (Locker)
                    VoicemailMessages.Add(
                        RestSharp.Contrib.HttpUtility.HtmlDecode(voicemailMessage.Id).ToLower(),
                        voicemailMessage);
            dynamic vMail = new ExpandoObject();
            vMail.Id = voicemailMessage.Id;
            vMail.Subject = voicemailMessage.Subject;
            VMailRest.AddVmail(vMail);
        }

        protected override string Id
        {
            get { return "8AC8CE24-D023-4DE8-890D-25019DC910AC"; }
        }

        protected override string DisplayName
        {
            get { return "Visual VMail"; }
        }

        protected override string CategoryId
        {
            get { return "090EAC44-1B0E-4970-B7B5-E64374AF4214"; }
        }

        protected override string CategoryDisplayName
        {
            get { return "VisualVmail"; }
        }

        public override object Content
        {
            get { return new MainView(); }
        }

        public static void StartMonitoringVmails()
        {
            if (string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Password)) return;
            VMailRest = new VMailRestHelper(UserName, Password);
            _uMm = UnifiedMessagingManager.GetInstance(_session);
            _uMm.NewVoicemail += UMmOnNewVoicemail;
            _uMm.VoicemailDeleted += UMmOnVoicemailDeleted;

            if (_uMm.Voicemails.Any())
                Task.Factory.StartNew(() =>
                {

                    try
                    {

                        foreach (var voicemail in _uMm.Voicemails.Where(p => !p.Subject.Equals("IC Voicemail: Voicemail Notification")))
                        {
                            AddMessage(voicemail);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                });

            CheckCommands.Elapsed += CheckCommandsOnElapsed;

            CheckCommands.Start();
            RefreshVmailTimer.Start();
            try
            {
                _uMm.RefreshVoicemailCache(-1);
            }
            catch (RateLimitedException e)
            {
                //Console.WriteLine(e);
            }
        }

        private static void CheckCommandsOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            try
            {
                CheckCommands.Stop();
                if (!VoicemailMessages.Any())
                {
                    CheckCommands.Start();
                    return;
                }
                var request = VMailRest.GetCallRequests();
                if (!string.IsNullOrEmpty(request.PhoneNumber) && !string.IsNullOrEmpty(request.VmailId))
                {

                    try
                    {
                        var playVmail = VoicemailMessages[request.VmailId];
                        playVmail.PlayToNumber(playVmail.Attachments[0], request.PhoneNumber, true, false);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                    finally
                    {
                        CheckCommands.Start();
                    }
                }
                CheckCommands.Start();
            }
            catch (Exception ex) { }
            finally
            {
                CheckCommands.Start();
            }
        }

        private static void UMmOnVoicemailDeleted(object sender, VoicemailEventArgs voicemailEventArgs)
        {
            VoicemailMessages.Remove(voicemailEventArgs.Message.Id);
            Task.Factory.StartNew(() =>
            {

                try
                {
                    dynamic vMail = new ExpandoObject();
                    vMail.Id = voicemailEventArgs.Message.Id;
                    vMail.Subject = voicemailEventArgs.Message.Subject;
                    VMailRest.DeleteVmail(vMail);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            });
        }

        public static void StopMonitoringVmails()
        {
            _uMm.NewVoicemail -= UMmOnNewVoicemail;
            CheckCommands.Elapsed -= CheckCommandsOnElapsed;
            CheckCommands.Stop();
            RefreshVmailTimer.Stop();
        }

        protected override void OnLoad(IServiceProvider serviceProvider)
        {
            _trace = serviceProvider.GetService(typeof(ITraceContext)) as ITraceContext;
            try
            {
                CallService = serviceProvider.GetService(typeof (ICallService)) as ICallService;
                _session = serviceProvider.GetService(typeof (Session)) as Session;
                if (_session != null) 
                    _session.ConnectionStateChanged += SessionOnConnectionStateChanged;
                //uMm = serviceProvider.GetService(typeof(UnifiedMessagingManager)) as UnifiedMessagingManager;
            }
            catch (Exception ex)
            {
                if (_trace != null) _trace.Error(string.Format("Unable to load ICallService\r\n{}", ex.Message));
            }

        }

        private void SessionOnConnectionStateChanged(object sender, ConnectionStateChangedEventArgs connectionStateChangedEventArgs)
        {
            if (connectionStateChangedEventArgs.State == ConnectionState.Down || connectionStateChangedEventArgs.State == ConnectionState.None)
                StopMonitoringVmails();
            if (connectionStateChangedEventArgs.State == ConnectionState.Up) 
                StartMonitoringVmails();
        }

        public static bool PlayMessageToNumber(string userName, string messageId, string phoneNumber, Session session)
        {

            
            if (String.IsNullOrEmpty(messageId)) return false;
            if (String.IsNullOrEmpty(userName)) return false;
            if (String.IsNullOrEmpty(phoneNumber)) return false;
            var dispName = String.Empty;
            var unifiedMessageManager = _uMm;

            foreach (var entry in VoicemailMessages.Where(entry =>
                String.Equals(HttpUtility.HtmlDecode(entry.Value.Id), HttpUtility.HtmlDecode(messageId), StringComparison.CurrentCultureIgnoreCase)))
            {
                var entry2 = entry;
                var phoneNumber2 = phoneNumber;
                Task.Factory.StartNew(
                    () =>
                        entry2.Value.PlayToNumber(entry2.Value.Attachments.First(p => p.Name.Contains("wav")),
                            phoneNumber2,
                            true, true));
                return true;
            }
            return false;
        }
    }


    
}
